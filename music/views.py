# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,get_object_or_404
from .models import Album,Song



def index(request):
	#Cogemos todos los albumos de la base de datos
	all_albums = Album.objects.all()
	#Realizamos un context con los datos para poder mostrarlos en la vista con un for
	context = { 'all_albums': all_albums}
	return render(request,'index.html',context)

def detail(request,album_id):
	album = get_object_or_404(Album.objects, id=album_id)
	return render(request,'details.html',{'album': album})


def favorite(request,album_id):
	album = get_object_or_404(Album.objects, id=album_id)
	try:
		selected_song = album.song_set.get(id=request.POST['song'])
	except (KeyError, Song.DoesNotExist):
		return render(request,'details.html',{
				'album': album,
				'error_message': "Tried to favorite, but something gone wrong",
			})
	else:
		selected_song.favorite = not  selected_song.favorite
		selected_song.save()
		return render(request,'details.html',{'album': album})
